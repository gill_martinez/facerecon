# -*- coding: utf-8 -*-
import serial
import math
import time
import struct

from tago import Tago

class TagoRequests(object):
    def __init__(self):
        self.my_device_token = '976b1cd6-de44-4189-802e-2c7d00bc02e3'
        self.my_device = Tago(self.my_device_token).device 
    
    def sendData(self, number_people, string):
        print('sending Data To Tago..')
        #self.dados = number_people
        self.pyload=[{"variable":"pessoas",
                      "value":number_people,
                      "unit":"int"
                      },
                      {"variable":"nome",
                      "value":string,
                      "unit":"string",
                      }]
        self.response = self.my_device.insert(self.pyload)

    def getData(self):
        print('receiving Data From Tago..')

    def updateDatabase(self):
        print('updating mongoDB')

    def deleteData(self):
        self.result = self.my_device.api_data_delete('Bucket')
        if self.result['status']:
            print('Data removed')
        else:
            print(self.result['message'])
