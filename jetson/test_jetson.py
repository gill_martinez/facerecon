import cv2
import numpy as np 
from time import sleep

def video_streaming():
    gst = "nvcamerasrc ! video/x-raw(memory:NVMM), width=(int)1280, " + \
	  "height=(int)720,format=(string)I420, framerate=(fraction)30/1 ! " + \
	  " nvvidconv flip-method=4 ! video/x-raw, format=(string)BGRx ! " + \
	  " videoconvert ! video/x-raw, format=(string)BGR ! appsink"
    
    cap = cv2.VideoCapture(gst)
    
    print("setting up camera...")

    #sleep is because the camera takes some time to adapt itself to the light
    sleep(1)

    print("capturing images")

    cv2.namedWindow('tx1Cam')

    while True:
        ret, frame = cap.read() #return a single frame in variable 'frame'
        cv2.imshow('Video',frame) #display the captured frame

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
        
    #when everything is done, release the capture
    cap.release()
    cv2.destroyAllWindows()

    


if __name__ == "__main__":
    video_streaming()