import cv2
import face_recognition
import sys
from tagoFunctions import TagoRequests
import numpy as np
import glob
import MySQLdb

from PIL import Image
import base64
import cStringIO
import PIL.Image



def open_database():
    #Conecta no Banco de Dados
    #Define variaveis para conexao com DB
    host     = "localhost"
    user     = "username"
    password = "password"
    database = "ProjetosB" #nome do BD
    # Abre conexao com banco de dados
    db = MySQLdb.connect(host,user,password,database)

    print ("conectou banco")
    return db

def close_database(db):
    db.close


def delete_rows(db, cursor):
    cursor.execute("DELETE FROM pessoas")

def load_pictures(db, cursor):
    known_face_encodings = []
    known_face_names     = []
    fixed_name = "_image_encoding"
    
    for filename in glob.glob('pictures/*.jpeg'):
        print(filename)
        image = face_recognition.load_image_file(filename)
        image_encode = face_recognition.face_encodings(image)[0]
        line = filename.split("/")
        print(line)
        name = line[1].split(".")[0]

        print(name)
        
        image = Image.open(filename)
        cursor.execute("INSERT INTO pessoas(nome,presente,imagem) VALUES(%s,%s,%s)",(name,"Ausente",image))        
        
        known_face_encodings.append(image_encode)
        known_face_names.append(name)
        
    db.commit()
        
    return known_face_encodings, known_face_names


def captureStreaming():
    number_people = 0
    tagoObj = TagoRequests()
    #tagoObj.deleteData()

    db = open_database()
    cursor = db.cursor(MySQLdb.cursors.DictCursor)

    known_face_encodings, known_face_names = load_pictures(db, cursor)

    video_capture = cv2.VideoCapture(0)

  # Initialize some variables
    face_locations = []
    face_encodings = []
    face_names = []

    process_this_frame = True
    frame_counter = 0   
    
    while True:

        
        ret, frame = video_capture.read()

        # Resize frame of video to 1/4 size for faster face recognition processing
        small_frame = cv2.resize(frame, (0, 0), fx=0.25, fy=0.25)

        # Convert the image from BGR color (which OpenCV uses) to RGB color (which face_recognition uses)
        rgb_small_frame = small_frame[:, :, ::-1]

        # Only process every other frame of video to save time
        if process_this_frame:
            # Find all the faces and face encodings in the current frame of video
            face_locations = face_recognition.face_locations(rgb_small_frame)
            face_encodings = face_recognition.face_encodings(rgb_small_frame, face_locations)

            face_names = []
            for face_encoding in face_encodings:
                # See if the face is a match for the known face(s)
                matches = face_recognition.compare_faces(known_face_encodings, face_encoding)
                name = "Unknown"

                # If a match was found in known_face_encodings, just use the first one.
                if True in matches:
                    first_match_index = matches.index(True)
                    name = known_face_names[first_match_index]
                    #print(name)
                    
                    cursor.execute("select presente from pessoas where nome = '%s'" %name)
                    resultado = cursor.fetchall()
                    for linha in resultado:
                        resultado2 = linha["presente"]
                        
                    if (resultado2 == 'Ausente'):
                        cursor.execute("UPDATE pessoas set presente = 'Presente' where nome = '%s'" %name)
                        db.commit()#usa sempre que for colocar/modificar algo no banco
                        number_people += 1 
                        tagoObj.sendData(number_people, name)     
                
                #face_names.append(name)
                print(number_people)
                

        if(frame_counter == 3):
            process_this_frame = True
            frame_counter = 0
        else:
            process_this_frame = False 

        frame_counter +=1

        # Display the results
        for (top, right, bottom, left), name in zip(face_locations, face_names):
            # Scale back up face locations since the frame we detected in was scaled to 1/4 size
            top *= 4
            right *= 4
            bottom *= 4
            left *= 4
        
            #Draw a box around the face
            cv2.rectangle(frame, (left, top), (right, bottom), (0, 0, 255), 2)

            # Draw a label with a name below the face
            cv2.rectangle(frame, (left, bottom - 35), (right, bottom), (0, 0, 255), cv2.FILLED)
            font = cv2.FONT_HERSHEY_DUPLEX
            cv2.putText(frame, name, (left + 6, bottom - 6), font, 1.0, (255, 255, 255), 1)
        
        cv2.imshow('Video', frame)

        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

    #when everything is done, release the capture
    video_capture.release()
    cv2.destroyAllWindows()
    
    #delete all rows from table pessoas
    delete_rows(db, cursor)
    
    close_database(db)
    cursor.close()

def sendTago(self):
    
    print("sending tago") 


if __name__ == "__main__":
    tagoObj = TagoRequests()
    tagoObj.sendData('0', "SALA VAZIA") 
    print("Iniciou") 
    #db = open_database()
    #cursor = db.cursor(MySQLdb.cursors.DictCursor)
    #load_pictures(db, cursor)
    captureStreaming()
