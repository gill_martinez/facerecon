import cv2
import sys

cascPath = sys.argv[1]

video_capture = cv2.VideoCapture(0)

faceCascade = cv2.CascadeClassifier(cascPath)

while True:
    ret, frame = video_capture.read()
    
    #COLOR_RGBA2RGB
    gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)

    faces = faceCascade.detectMultiScale(
        gray,
        scaleFactor = 1.1,
        minNeighbors = 5,
        minSize = (30,30),
        #flags
    )
    
    # Draw a rectangle around the faces
    for (x, y, w, h) in faces:
        cv2.rectangle(frame, (x, y), (x+w, y+h), (0, 255, 0), 2)


    cv2.imshow('frame', frame)

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

#when everything is done, release the capture
video_capture.release()
cv2.destroyAllWindows()
