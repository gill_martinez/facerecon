import MySQLdb

#Conecta no Banco de Dados
#Define variaveis para conexao com DB
host     = "localhost"
user     = "root"
password = "nvidia"
database = "banco" #nome do BD


# Abre conexao com banco de dados
db = MySQLdb.connect(host,user,password,database)

#Cria objeto para realizar futuros comandos SQL
cursor = db.cursor(MySQLdb.cursors.DictCursor)

#inserir
with open('/home/pessoax.jpeg', 'rb') as image:
    imagem = image.read()
	
	cursor.execute("INSERT INTO pessoas(%s,%s,%s,%s,%s)",(id,nome,status,dia,imagem)) 
	
db.commit()#usa sempre que for colocar/modificar algo no banco

#alterar
cursor.execute("SELECT tempC from pessoas")#select não tem commit()  *= TUDO, no lugar poderia colocar um espaço em especifico
resultado = cursor.fetchall()#resultado recebe uma matriz com valores

for linha in resultado:
   temperatura = linha["tempC"]
   print temperatura

CREATE TABLE pessoas (
  id int NOT NULL AUTO_INCREMENT,
  nome varchar(225) NOT NULL,
  status varchar(225) NOT NULL default 'ausente',
  dia datetime DEFAULT CURRENT_TIMESTAMP,
  imagem longblob NOT NULL
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
